# BuildROM script

Complex system for ROM maintainers

##Features:
* Setup local enviroment
* Syncing and update ROM sources
* Source cleanup
* Smart ROM
* And more others features for maintainers

##Using:
For start using BuildROM tou need download it to ~/BuildROM/Build.sh


    mkdir -p ~/BuildROM 
    cd ~/BuildROM
    wget https://gitlab.com/MrYacha/buildrom/blob/master/Build.sh Build.sh
    bash Build.sh

